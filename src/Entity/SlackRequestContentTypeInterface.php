<?php

namespace Drupal\slack\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Slack request content type entities.
 */
interface SlackRequestContentTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
