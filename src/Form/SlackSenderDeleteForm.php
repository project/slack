<?php

namespace Drupal\slack\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Slack sender entities.
 *
 * @ingroup slack
 */
class SlackSenderDeleteForm extends ContentEntityDeleteForm {


}
