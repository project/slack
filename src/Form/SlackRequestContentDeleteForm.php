<?php

namespace Drupal\slack\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Slack request content entities.
 *
 * @ingroup slack
 */
class SlackRequestContentDeleteForm extends ContentEntityDeleteForm {


}
