<?php

namespace Drupal\slack\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Slack App entities.
 *
 * @ingroup slack
 */
class SlackAppDeleteForm extends ContentEntityDeleteForm {


}
