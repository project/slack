<?php

namespace Drupal\slack;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for slack_request_content.
 */
class SlackRequestContentTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
